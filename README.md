# vib2_driver

The repository contains the userspace Python library and ROS Driver for communciating with the Raven Vehicle Interface Board 2.0.

Primary documentation for these tools is in the [Raven Wiki.](https://gitlab.com/apl-ocean-engineering/raven/raven_wiki/-/wikis/Hardware/Vehicle-Interface-Board-2.0).

This repo contains three disparate-ish functions:

* A Python library ([vib2_driver](https://gitlab.com/apl-ocean-engineering/raven/vib2_driver/-/tree/main/lib/vib2_driver)) for communicating with the VIB.   As the VIB interface is via HTTP, these functions are largely thin wrappers for constructing command URLs and parsing the responses.
* Command line tools for controlling the VIB (e.g. enabling VPSUs) which use the Python library to communicate directly with the VIB over HTTP (not through the ROS node)
* A ROS node [vib2](https://gitlab.com/apl-ocean-engineering/raven/vib2_driver/-/blob/main/nodes/vib2) which polls status information the VIB.

For more details see the [VIB2 pages in the Raven wiki.](https://gitlab.com/apl-ocean-engineering/raven/raven_wiki/-/wikis/Hardware/Vehicle-Interface-Board-2.0).

Firmware for the VIB2.0 is at [vib2_tibbo_firmware](https://gitlab.com/apl-ocean-engineering/raven/vib2_tibbo_firmware).


# License

This code is released under the [BSD 3-Clause license](LICENSE)
