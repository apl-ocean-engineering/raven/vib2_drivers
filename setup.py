# ! DO NOT MANUALLY INVOKE THIS setup.py, USE CATKIN INSTEAD
from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

# fetch values from package.xml
setup_args = generate_distutils_setup(
    requires=["requests", "json"],
    packages=["vib2_driver"],
    scripts=["nodes/vib2"],
    package_dir={"": "lib"},
    entry_points={  # Optional
        "console_scripts": [
            "vpsu=vib2_driver.vpsu_control:main",
            "a10switch=vib2_driver.switch_control:main",
        ],
    },
)
setup(**setup_args)
