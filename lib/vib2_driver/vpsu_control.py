#!/usr/bin/env python3

import json
import argparse

from vib2_driver.vib2_http_client import Vib2HttpClient


class VpsuControl(Vib2HttpClient):
    # n.b. these mappings are **NOT** correct right now
    SHORTCUTS = {
        "all": [],
        "thrusters": ["vpsu1", "vpsu3"],
        "a8": ["vpsu1", "vpsu2"],
        "a9": ["vpsu3", "vpsu4"],
    }

    def __init__(self, host):
        super().__init__(host)

    def status(self):
        return json.loads(self.get("vp.json"))

    def parse_circuits(self, circuits):
        stat = self.status()

        # Translate the meta-circuits
        real_circuits = []
        for c in circuits:
            if c in stat.keys():
                real_circuits.append(c)
            elif c == "all":
                # Handle this as a special exception
                real_circuits = list(stat.keys())
            elif c in VpsuControl.SHORTCUTS.keys():
                real_circuits += VpsuControl.SHORTCUTS[c]
            else:
                raise ValueError('Don\'t understand circuit "%s"' % c)

        return list(set(real_circuits))

    def enable(self, circuits):
        real_circuits = self.parse_circuits(circuits)

        print("Enabling: %s" % real_circuits)

        http_args = ["%s=ON" % c for c in real_circuits]
        self.get("vp.json", http_args)
        return self.status()

    def disable(self, circuits):
        real_circuits = self.parse_circuits(circuits)

        print("Disabling: %s" % real_circuits)

        http_args = ["%s=OFF" % c for c in real_circuits]
        self.get("vp.json", http_args)
        return self.status()


# ~~~~~~~~~~~~~~~~~~~~~~~~~ Command line client ~~~~~~~~~~~~~~~~~~~~


def print_status(status):
    for key, val in status.items():
        print("%s : %6s" % (key, val))


def cli_status(args, vpsu):
    status = vpsu.status()
    print_status(status)


def cli_enable(args, vpsu):
    s = vpsu.enable(args.circuits)
    print_status(s)


def cli_disable(args, vpsu):
    s = vpsu.disable(args.circuits)
    print_status(s)


def main():
    parser = argparse.ArgumentParser(
        description="Control VPSU enables on the Raven VIB2"
    )

    # Global argparse arguments
    default_ip = "vib.raven.apl.uw.edu"  # "10.10.10.134"
    parser.add_argument(
        "--host",
        default=default_ip,
        help="Hostname/IP address for Netburner (default: %s)" % default_ip,
    )

    subparser = parser.add_subparsers(required=True, dest="command")

    subparser.add_parser(
        "status", help="Returns the current status of the VPSU enables"
    ).set_defaults(func=cli_status)

    output_parser = subparser.add_parser(
        "enable", help="Enable one or more VPSU circuits"
    )
    output_parser.set_defaults(func=cli_enable)
    output_parser.add_argument(
        "circuits",
        nargs="*",
        default=["thrusters"],
        help='Circuits to enable (defaults to "thrusters").  Can be a circuit name or one of these shortcuts: %s'
        % (", ".join(VpsuControl.SHORTCUTS.keys())),
    )

    output_parser = subparser.add_parser(
        "disable", help="Disable one or more VPSU circuits"
    )
    output_parser.set_defaults(func=cli_disable)
    output_parser.add_argument(
        "circuits",
        nargs="*",
        default=["all"],
        help='Circuits to enable (defaults to "all").  Can be a circuit name or one of these shortcuts: %s'
        % (", ".join(VpsuControl.SHORTCUTS.keys())),
    )

    args = parser.parse_args()

    vpsu = VpsuControl(host=args.host)
    args.func(args, vpsu)
