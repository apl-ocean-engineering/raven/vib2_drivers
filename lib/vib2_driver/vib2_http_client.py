#!/usr/bin/env python3

import requests


class Vib2HttpClient:
    def __init__(self, host):
        self.host = host
        self.port = 80

    def get(self, path, args=None):

        if args:
            url = "http://%s:%d/%s?%s" % (self.host, self.port, path, "&".join(args))
        else:
            url = "http://%s:%d/%s" % (self.host, self.port, path)

        s = requests.get(url, timeout=3)

        if s.status_code != 200:
            raise requests.exceptions.InvalidURL()

        return s.text
