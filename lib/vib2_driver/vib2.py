#!/usr/bin/env pyyhon3

from vib2_driver.leak import LeakControl
from vib2_driver.seapoint import SeapointControl


# An omnibus struct for holding a set of VIB2 interfaces
#
# Used by the ROS node
class Vib2Driver:
    def __init__(self, host):
        self._leak = LeakControl(host)
        self._seapoint = SeapointControl(host)

    @property
    def leak(self):
        return self._leak

    @property
    def seapoint(self):
        return self._seapoint
