#!/usr/bin/env python3

import argparse
import json
from vib2_driver.vib2_http_client import Vib2HttpClient


class SwitchControl(Vib2HttpClient):
    def __init__(self, host):
        super().__init__(host)

    def status(self):
        return json.loads(self.get("sw.json"))

    def parse_circuits(self, circuits):
        stat = self.status()

        # Translate the meta-circuits
        real_circuits = []
        for c in circuits:
            if c in stat.keys():
                real_circuits.append(c)
            else:
                raise ValueError('Don\'t understand circuit "%s"' % c)

        return list(set(real_circuits))

    def enable(self, circuits):
        real_circuits = self.parse_circuits(circuits)

        print("Enabling: %s" % real_circuits)

        http_args = ["%s=ON" % c for c in real_circuits]
        self.get("sw.json", http_args)
        return self.status()

    def disable(self, circuits):
        real_circuits = self.parse_circuits(circuits)

        print("Disabling: %s" % real_circuits)

        http_args = ["%s=OFF" % c for c in real_circuits]
        self.get("sw.json", http_args)
        return self.status()


# ~~~~~~~~~~~~~~~~~~ Command line client ~~~~~~~~~~~~~~~~~~~


def print_status(status):
    for key, val in status.items():
        print("%-8s : %-6s" % (key, val))


def cli_status(args, sw):
    status = sw.status()
    print_status(status)


def cli_enable(args, sw):
    s = sw.enable(args.circuits)
    print_status(s)


def cli_disable(args, sw):
    s = sw.disable(args.circuits)
    print_status(s)


def main():
    parser = argparse.ArgumentParser(description="Control sw enables on the Raven VIB2")

    # Global argparse arguments
    default_ip = "vib.raven.apl.uw.edu"  # "10.10.10.134"
    parser.add_argument(
        "--host",
        default=default_ip,
        help="Hostname/IP address for Netburner (default: %s)" % default_ip,
    )

    subparser = parser.add_subparsers(required=True, dest="command")

    subparser.add_parser(
        "status", help="Returns the current status of the switch enables"
    ).set_defaults(func=cli_status)

    output_parser = subparser.add_parser(
        "enable", help="Enable one or more sw circuits"
    )
    output_parser.set_defaults(func=cli_enable)
    output_parser.add_argument(
        "circuits", nargs="*", help='Circuit(s) to enable (defaults to "thrusters")'
    )

    output_parser = subparser.add_parser(
        "disable", help="Disable one or more sw circuits"
    )
    output_parser.set_defaults(func=cli_disable)
    output_parser.add_argument(
        "circuits", nargs="*", help='Circuits to disable, or "all"'
    )

    args = parser.parse_args()

    sw = SwitchControl(host=args.host)
    args.func(args, sw)
