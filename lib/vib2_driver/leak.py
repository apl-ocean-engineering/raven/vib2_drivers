#!/usr/bin/env python3

import json
from vib2_driver.vib2_http_client import Vib2HttpClient


class LeakControl(Vib2HttpClient):
    def __init__(self, host):
        super().__init__(host)

    def read(self):
        return json.loads(self.get("lk.json"))


# ~~~~~~~~~~~~~~~~~~ Command line client ~~~~~~~~~~~~~~~~~~~

# def print_status(status):
#     for key,val in status.items():
#         print("%-8s : %-6s" % (key, val))

# def cli_status(args, sw):
#     status = sw.status()
#     print_status(status)

# def cli_enable(args, sw):
#     s = sw.enable(args.circuits)
#     print_status(s)

# def cli_disable(args, sw):
#     s = sw.disable(args.circuits)
#     print_status(s)

# def main():
#     import argparse
#     parser = argparse.ArgumentParser(description='Control sw enables on the Raven VIB2')

#     # Global argparse arguments
#     DEFAULT_IP = "172.31.12.39" # "10.10.10.134"
#     parser.add_argument("--host",
#                         default=DEFAULT_IP,
#                         help="Hostname/IP address for Netburner (default: %s)" % DEFAULT_IP)

#     subparser = parser.add_subparsers(required=True,dest='command')

#     subparser.add_parser('status',help="Returns the current status of the switch enables").set_defaults(func=cli_status)

#     output_parser = subparser.add_parser('enable', help="Enable one or more sw circuits")
#     output_parser.set_defaults( func=cli_enable )
#     output_parser.add_argument("circuits", nargs="*", default=["thrusters"], help="Circuits to enable (defaults to \"thrusters\").  Valid values: a8, a9, thrusters, arms, all")

#     output_parser = subparser.add_parser('disable', help="Disable one or more sw circuits")
#     output_parser.set_defaults( func=cli_disable )
#     output_parser.add_argument("circuits", nargs="*", default=["all"], help="Circuits to disable (default: \"all\").  Valid values: a8, a9, thrusters, arms, all")

#     args = parser.parse_args()

#     sw = SwitchControl(host=args.host)
#     args.func(args, sw)
